const Course = require("../models/Course");

//Create a new course

/*
	Steps:
	1. Create a new course object using the monggose model and the information from the reqBody and the id from the header.
	2. Save the new User to the database
*/

module.exports.addCourse = (data) =>{

    console.log(data);

	if (data.isAdmin){
		
    	let newCourse = new Course({
        	name : data.course.name,
        	description : data.course.description,
        	price : data.course.price
    		})

    console.log(newCourse);
    return newCourse.save().then((course,error)=>{
        if(error){
            return false;
        }else{
            return true;
        }
    })

    } else {
    	return false;
 }

}

//Retrieve all courses
/*
    1. Retrieve all the courses from the database

         Model.find({})
*/

module.exports.getAllCourses = ()=>{
    return Course.find({}).then(result=>{
        return result;
    });
};

//Retrieve all active courses
/*
    1. Retrieve all the courses from the database with the property of "isActive" to true.
*/

module.exports.getAllActive = ()=>{
    return Course.find({isActive:true}).then(result=>{
        return result;
    });
};

//Retrieving a specific course
/*
    1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (reqParams)=>{
    return Course.findById(reqParams.courseId).then(result=>{
        return result;
    });
};

//Updating a course
/*
    1. Create a variable "updatedCourse" which will contain the information retrieve from the request body.

    2. Find and update the course using the course ID retrieved from the request params property and the variable "updateCourse" containing the information from the request body.
*/

module.exports.updateCourse = (reqParams, reqBody)=>{

    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };

    return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
        if(error){
            return false;
        } else {
            return true;
        };
    });   
};


//ACTIVITY SOLUTION

//Archiving a course
module.exports.archiveCourse = (reqParams, reqBody)=>{

    let archivedCourse = {
        
        isActive: reqBody.isActive
    };

    return Course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error)=>{
        if(error){
            return false;
        } else {
            return true;
    };
});
};

